# Amherst College Department of Geology Scanning Electron Microscope (SEM) Lab Database Parsing

## **Background & Project Statement**
The Beneski Scanning Electron Microscope Lab faces the challenge of managing a substantial dataset comprising approximately 2 terabytes of images and point data originating from geological specimens collected worldwide. Research is ongoing, constantly increasing the size of the dataset. The data consists of a wide array of samples, ranging from microscopic rock formations to intricate metal structures, captured using:

Petrographic light microscopes:
- Zeiss Axio Imager.M2m
- Zeiss AxioScope 5

And a Zeiss Sigma 500 VP scanning electron microscope (SEM) and the following detectors:
- USB TV1 Camera
- InLens Secondary Electron Detector
- Secondary Electron (SE2) Detector
- Variable Pressure Secondary Electron (VPSE G4) Detector
- Backscatted Electron (HDBSD) Detector
- Delmic Jolt Cathodoluminescence (CL) Detector
- Oxford Instruments Ultim Max 100 Energy-Dispersive (EDS) Detector
- Oxford Instruments Symmetry S2 Electron Backscatter Diffraction (EBSD) Detector
- Oxford Instruments Wave500 Wavelength Dispersive X-Ray (WDS or WDX) Detector

The SEM database holds all of the data produced on the machine -- organized by project. The [PostgreSQL](https://www.postgresql.org/) database is administered and developed with [pgAdmin](https://www.pgadmin.org/). 

The database also catalogs standard reference materials for the SEM lab -- their certified values and how well we reproduce the values each time we run them. 

Here, we include parsing scripts to allow for easy reference material data upload and analysis scripts which work to analyize our reproducibility.


## **Proposed Solution**
This solution involves using a basic R code to:
1. Analyze reference materials
    - Upload accepted values
    - Upload SEM runs of reference materials
    - Analyize reproducibility over time

## Software
R 4.1.3

### R Dependency Libraries
 - tidyverse
 - dplyr
 - DBI
 - naniar
 - progress
 - dotenv
 - purr
 - RPostgres
 - stringr

## Contributing and Support
See something that could work cleaner or have a new feature idea, please feel free to contribute to the project!

If you have a suggestion that would make this better, please fork the repo and create a pull request. 

Here are the steps!
1) Fork the Project
2) Create your Branch 
3) Commit your Changes
4) Push to the Branch
5) Open a Pull Request

If you experience issues with the code, support can be sought by emailing hbrooks@amherst.edu.

## Authors and acknowledgment
Written by Hanna L Brooks. Last update: 2024.

## License
Code is licensed with a MIT License. See license section for more information.